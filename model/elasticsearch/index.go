package elasticsearch

const (
	eSIndexPrefix       = "micro-mall-"
	ESIndexSkuInventory = eSIndexPrefix + "sku-inventory"
	ESIndexShop         = eSIndexPrefix + "shop"
)

type SearchStoreSkuInventory struct {
	SkuCode       string `json:"sku_code"`
	Name          string `json:"name"`
	Title         string `json:"title"`
	SubTitle      string `json:"sub_title"`
	Desc          string `json:"desc"`
	Production    string `json:"production"`
	Color         string `json:"color"`
	Specification string `json:"specification"`
	ShopId        int64  `json:"shop_id"`
}

type SearchStoreShop struct {
	ShopId       int64  `json:"shop_id,omitempty"`
	NickName     string `json:"nick_name,omitempty"`
	FullName     string `json:"full_name,omitempty"`
	ShopCode     string `json:"shop_code,omitempty"`
	RegisterAddr string `json:"register_addr,omitempty"`
	BusinessAddr string `json:"business_addr,omitempty"`
	BusinessDesc string `json:"business_desc,omitempty"`
}

type SearchResponse struct {
	Took     int  `json:"took"`
	TimedOut bool `json:"timed_out"`
	Shards   struct {
		Total      int `json:"total"`
		Successful int `json:"successful"`
		Skipped    int `json:"skipped"`
		Failed     int `json:"failed"`
	} `json:"_shards"`
	Hits SearchHits `json:"hits"`
}

type SearchHits struct {
	Total struct {
		Value    int    `json:"value"`
		Relation string `json:"relation"`
	} `json:"total"`
	MaxScore float64 `json:"max_score"`
	Hits     []struct {
		Index  string                 `json:"_index"`
		Type   string                 `json:"_type"`
		ID     string                 `json:"_id"`
		Score  float64                `json:"_score"`
		Source map[string]interface{} `json:"_source"`
	} `json:"hits"`
}
