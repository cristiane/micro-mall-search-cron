package vars

type EmailConfigSettingS struct {
	Enable   bool   `json:"enable"`
	User     string `json:"user"`
	Password string `json:"password"`
	Host     string `json:"host"`
	Port     string `json:"port"`
}

type ElasticsearchSettingS struct {
	Addresses string `json:"addresses"`
	Username  string `json:"username"`
	Password  string `json:"password"`
}

type SyncShopInfoTaskSettingS struct {
	Cron string `json:"cron"`
}

type SyncSkuInfoTaskSettingS struct {
	Cron string `json:"cron"`
}
