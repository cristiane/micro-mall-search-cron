package startup

import (
	"gitee.com/cristiane/micro-mall-search-cron/vars"
	"gitee.com/kelvins-io/kelvins/config"
)

const (
	SectionEmailConfig         = "email-config"
	SectionElasticsearchConfig = "elasticsearch-config"
	SyncSkuInfoTaskConfig      = "sync-sku-info-task"
	SyncShopInfoTaskConfig     = "sync-shop-info-task"
)

// LoadConfig 加载配置对象映射
func LoadConfig() error {
	// 加载email数据源
	vars.EmailConfigSetting = new(vars.EmailConfigSettingS)
	config.MapConfig(SectionEmailConfig, vars.EmailConfigSetting)
	// 加载ES
	vars.ElasticsearchSetting = new(vars.ElasticsearchSettingS)
	config.MapConfig(SectionElasticsearchConfig, vars.ElasticsearchSetting)
	// 同步商品信息
	vars.SyncSkuInfoTaskSetting = new(vars.SyncSkuInfoTaskSettingS)
	config.MapConfig(SyncSkuInfoTaskConfig, vars.SyncSkuInfoTaskSetting)
	// 同步店铺信息
	vars.SyncShopInfoTaskSetting = new(vars.SyncShopInfoTaskSettingS)
	config.MapConfig(SyncShopInfoTaskConfig, vars.SyncShopInfoTaskSetting)
	return nil
}
