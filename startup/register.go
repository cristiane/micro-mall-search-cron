package startup

import (
	"gitee.com/cristiane/micro-mall-search-cron/service"
	"gitee.com/cristiane/micro-mall-search-cron/vars"
	"gitee.com/kelvins-io/kelvins"
)

const (
	CronStoreShopTask         = "0 */5 * * * *"
	CronStoreSkuInventoryTask = "30 */6 * * * *"
)

func GenCronJobs() []*kelvins.CronJob {
	tasks := make([]*kelvins.CronJob, 0)
	// 店铺，商品存储数据同步应该由业务方定时任务投递消息给搜素队列

	if vars.SyncSkuInfoTaskSetting != nil {
		if vars.SyncSkuInfoTaskSetting.Cron != "" {
			tasks = append(tasks, &kelvins.CronJob{
				Name: "同步店铺数据到 elasticsearch",
				Spec: CronStoreShopTask,
				Job:  service.ShopSearchSyncTask,
			})
		}
	}

	if vars.SyncShopInfoTaskSetting != nil {
		if vars.SyncShopInfoTaskSetting.Cron != "" {
			tasks = append(tasks, &kelvins.CronJob{
				Name: "同步商品库存数据到 elasticsearch",
				Spec: CronStoreSkuInventoryTask,
				Job:  service.SkuInventorySearchSyncTask,
			})
		}
	}

	return tasks
}
